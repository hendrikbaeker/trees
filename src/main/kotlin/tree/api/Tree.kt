package tree.api

import tree.api.configuration.AggregationConfig
import tree.api.configuration.RenderingConfig
import tree.common.Handle
import tree.common.Node
import tree.common.NodeRegister
import tree.common.TraversalStrategy


class Tree<I, P, A>(private var nodeRegister: NodeRegister<I, P?, A?>) {


    fun getNode(reference: String): Node<I, P?, A?>? {
        return nodeRegister.getByReference(reference)
    }


    fun getRoot(): Node<I, P?, A?> {
        return nodeRegister.getById(Handle.ROOT_HANDLE.id)
                ?: throw java.lang.IllegalArgumentException("Tree does not have a root.")
    }


    fun aggregate(aggregationConfig: AggregationConfig<I, P?, A?>,
                  node: Node<I, P?, A?> = getRoot()) {
        node.children.forEach { aggregate(aggregationConfig, it) }
        node.aggregationInfo = aggregationConfig.computeNodeAggregationInfo(node)
    }

    fun <R> render(renderingConfig: RenderingConfig<I, P?, A?, R>,
                   node: Node<I, P?, A?> = getRoot()): RenderedNode<R> {

        val renderedNode = renderingConfig.renderNode(node)
        renderedNode.children = node.children.map { render(renderingConfig, it) }
        return renderedNode

    }

    fun attachTreeTo(userRef: String, attachableTree: Tree<I, P?, A?>) {

        if (attachableTree === this) {
            throw IllegalArgumentException("Tree cannot be attached to itself, this will a non-cycle-free graph.")
        }

        val attachee = nodeRegister.getByReference(userRef)
                ?: throw IllegalArgumentException("Trying to attach a tree to non-existing reference " + userRef)
        val attachableRoot = attachableTree.getRoot()

        attachableRoot.children.forEach {
            it.parent = attachee
            traverseNodeAndRun(it) { node ->
                run {
                    node.prefaceHandleWith(attachee.handle)
                    nodeRegister.add(node)
                }
            }
        }
        attachee.children = attachableRoot.children
    }

    fun stripTreeToFirstTwoLevels(): Tree<I, P, A> {

        val newRoot = getRoot().copyNodeWithoutChildren(null)
        newRoot.children = getRoot().children.map { it.copyNodeWithoutChildren(newRoot) }
        val newNodeRegister = NodeRegister<I, P?, A?>(newRoot)
        newRoot.children.forEach { newNodeRegister.add(it) }
        return Tree(newNodeRegister)
    }


    fun sortChildren() {
        traverseNodeAndRun(runnable = { node ->
            run {
                node.children = node.children.sorted()
            }
        })
    }

    private fun traverseNodeAndRun(node: Node<I, P?, A?>? = getRoot(),
                                   traversalStrategy: TraversalStrategy = TraversalStrategy.DO_AND_DOWN,
                                   runnable: (Node<I, P?, A?>) -> Unit) {

        when (traversalStrategy) {
            TraversalStrategy.DOWN_AND_DO -> {
                node?.children?.forEach { traverseNodeAndRun(it, runnable = runnable) }
                node?.let { runnable.invoke(it) }
            }
            TraversalStrategy.DO_AND_DOWN -> {
                node?.let { runnable.invoke(it) }
                node?.children?.forEach { traverseNodeAndRun(it, runnable = runnable) }
            }
        }
    }

    fun prettyPrint(renderer: (Node<I, P?, A?>) -> String) {
        getRoot().prettyPrint(renderer)
    }

    fun genericEquals(other: Tree<I, P?, A?>, comparator: (Node<I, P?, A?>, Node<I, P?, A?>) -> Boolean): Boolean {

        val thisRoot = this.getRoot()
        val thatRoot = other.getRoot()

        return genericEquals(thisRoot, thatRoot, comparator)
    }

    private fun genericEquals(first: Node<I, P?, A?>,
                              second: Node<I, P?, A?>,
                              comparator: (Node<I, P?, A?>, Node<I, P?, A?>) -> Boolean): Boolean {

        if (first.children.size != second.children.size) {
            return false
        }

        return comparator.invoke(first, second) &&
                IntRange(0, first.children.size - 1).map {
                    val childA = first.children[it]
                    val childB = second.children[it]
                    genericEquals(childA, childB, comparator)
                }.all { it }

    }

    fun countChildren():Int{
        return getRoot().getAllSuccessors().size
    }

}
