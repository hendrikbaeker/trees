package tree.api

import tree.common.Hashing
import tree.common.Node
import tree.api.configuration.ComparableParentTreeExtractor
import tree.api.configuration.BasicParentTreeExtractor
import tree.api.configuration.PartitionTreeExtractor
import tree.common.Handle
import tree.common.SortingByComparatorKey
import tree.common.SortingByIndexKey
import tree.parent.ParentRawNode
import tree.parent.ParentTreeFactory
import tree.partition.PartitionTreeFactory
import tree.partition.PartitionRawNode
import java.lang.IllegalArgumentException

object TreeFactory {

    fun <I, P, A, E> buildParentTree(entities: Set<E>,
                                     extractor: ComparableParentTreeExtractor<I, P, E>): Tree<I, P?, A?> {

        val preparedEntities = entities.map {
            val key = extractor.extractComparableKey(it)
            val nodeInformation = extractor.extractNodeInformation(it)
            val parent = extractor.extractParentReference(it)
            val payload = extractor.extractPayload(it)

            ParentRawNode<I, P?>(key, parent, nodeInformation, payload)

        }.toSet()

        val rootNode = Node<I, P?, A?>(Handle.ROOT_HANDLE, extractor.provideRootNodeInfo())

        return ParentTreeFactory<I, P?, A?>(preparedEntities, rootNode).buildTree()
    }

    fun <I, P, A, E> buildParentTree(entities: Set<E>,
                                     extractor: BasicParentTreeExtractor<I, P, E>,
                                     comparator: Comparator<E>): Tree<I, P?, A?> {

        val preparedEntities = entities.map {

            val key = SortingByComparatorKey(extractor.extractBasicKey(it), it, comparator)
            val nodeInformation = extractor.extractNodeInformation(it)
            val parent = extractor.extractParentReference(it)
            val payload = extractor.extractPayload(it)

            ParentRawNode<I, P?>(key, parent, nodeInformation, payload)

        }.toSet()


        val rootNode = Node<I, P?, A?>(Handle.ROOT_HANDLE, extractor.provideRootNodeInfo())

        return ParentTreeFactory<I, P?, A?>(preparedEntities, rootNode).buildTree()
    }

    fun <I, P, A, E> buildParentTree(entities: List<E>,
                                     extractor: BasicParentTreeExtractor<I, P, E>): Tree<I, P?, A?> {
        val preparedEntities = entities.mapIndexed { index, it ->

            val key = SortingByIndexKey(extractor.extractBasicKey(it), index)
            val nodeInformation = extractor.extractNodeInformation(it)
            val parent = extractor.extractParentReference(it)
            val payload = extractor.extractPayload(it)

            ParentRawNode<I, P?>(key, parent, nodeInformation, payload)

        }.toSet()

        val rootNode = Node<I, P?, A?>(Handle.ROOT_HANDLE, extractor.provideRootNodeInfo())

        return ParentTreeFactory<I, P?, A?>(preparedEntities, rootNode).buildTree()
    }

    fun <I, P, A, E> buildPartitionTree(entities: Set<E>,
                           extractor: PartitionTreeExtractor<I, P, E>): Tree<I, P?, A?> {

        val preparedEntities = entities.map {
            val extractedKeys = extractor.extractKeys(it)
            val extractPayload = extractor.extractPayload(it)
            PartitionRawNode<P?>(extractedKeys, extractPayload)
        }.toSet()

        val nodeInformationMap = mutableMapOf<String, I>()
        nodeInformationMap[Hashing.LOCAL_ROOT_NODE_REFERENCE] = extractor.provideRootNodeInfo()

        entities.forEach {
            val extractedNodeInfos = extractor.extractNodeInfo(it)
            val extractedKeys = extractor.extractKeys(it)

            if (extractedKeys.size != extractedNodeInfos.size) {
                throw IllegalArgumentException("At least one of the entities provides different numbers of node keys and information" +
                        "bits. entityKeys=" + extractedKeys.joinToString("/"))
            }

            extractedKeys.mapIndexed { index, key ->
                val localReference = Hashing.computeLocallyUniqueReference(index + 1, key.extractReference())
                nodeInformationMap[localReference] = extractedNodeInfos[index]
            }
        }

        val root = Node<I, P?, A?>(Handle.ROOT_HANDLE, extractor.provideRootNodeInfo())

        return PartitionTreeFactory<I, P?, A?>(preparedEntities, nodeInformationMap, root).buildTree()
    }
}
