import tree.common.Node
import tree.api.RenderedNode
import java.io.OutputStream
import java.util.ArrayList

object TreeUtils {

    /**
     * Prints a formatted version of the tree to the specified output stream. For debugging this may simply be System::out. Note that
     * the output requires UTF-8 support, which may be a little shaky in Windows.
     */
    fun <R> prettyPrintRenderedNode(node: RenderedNode<R>,
                                    renderer: (RenderedNode<R>) -> String,
                                    outputStream: OutputStream) {

        prettyPrint(node, renderer, { noe -> noe.children ?: listOf() }, outputStream, listOf(), true, true)
    }

    /**
     * Prints a formatted version of the tree to the specified output stream. For debugging this may simply be System::out. Note that
     * the output requires UTF-8 support, which may be a little shaky in Windows.
     */
    fun <I, P, A> prettyPrintNode(node: Node<I, P?, A?>,
                                  renderer: (Node<I, P?, A?>) -> String,
                                  outputStream: OutputStream) {

        prettyPrint(node, renderer, { noe -> noe.children }, outputStream, listOf(), true, true)
    }

    private fun <X> prettyPrint(node: X,
                                renderer: (X) -> String,
                                childExtractor: (X) -> List<X>,
                                outputStream: OutputStream,
                                hasMoreChildren: List<Boolean>,
                                isLast: Boolean,
                                isRoot: Boolean) {
        val booleans: ArrayList<Boolean>
        if (isRoot) {
            outputStream.write(renderer.invoke(node).toByteArray())
            outputStream.write("\n".toByteArray())
            booleans = ArrayList()
        } else {
            val renderedChildLine = (hasMoreChildren.map { if (it) "|  " else "   " }
                    .joinToString("")
                    + (if (isLast) "└──" else "├──")
                    + renderer.invoke(node))
            outputStream.write(renderedChildLine.toByteArray())
            outputStream.write("\n".toByteArray())
            booleans = ArrayList(hasMoreChildren)
            booleans.add(!isLast)
        }
        val noChildren = childExtractor.invoke(node).size
        for (i in 0..noChildren - 2) {
            val ithChild = childExtractor.invoke(node)[i]
            prettyPrint(ithChild, renderer, childExtractor, outputStream, booleans, false, false)
        }
        if (noChildren != 0) {
            val lastChild = childExtractor.invoke(node)[noChildren - 1]
            prettyPrint(lastChild, renderer, childExtractor, outputStream, booleans, true, false)
        }
    }
}
