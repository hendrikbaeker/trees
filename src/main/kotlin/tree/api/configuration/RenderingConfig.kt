package tree.api.configuration

import tree.common.Node
import tree.api.RenderedNode

interface RenderingConfig< I, P, A, R> {

    fun renderNode(node: Node<I, P?, A?>): RenderedNode<R>

}
