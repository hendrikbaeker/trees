package tree.api.configuration

import tree.common.Node

interface AggregationConfig< I, P, A> {

    fun computeNodeAggregationInfo(node: Node<I, P, A>): A

}
