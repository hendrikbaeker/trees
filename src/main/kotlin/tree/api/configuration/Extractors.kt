package tree.api.configuration

import tree.common.Key

interface ParentTreeExtractor<I, P, E> {

    fun extractNodeInformation(entity: E):I

    fun extractPayload(entity: E):P?

    fun extractParentReference(entity: E):String?

    fun provideRootNodeInfo():I
}

interface BasicParentTreeExtractor<I, P,  E>: ParentTreeExtractor<I, P,  E> {

    fun extractBasicKey(entity:E):String
}

interface ComparableParentTreeExtractor< I, P,  E>: ParentTreeExtractor<I, P,  E> {

 fun extractComparableKey(entity:E):Key
}

interface PartitionTreeExtractor< I, P, E> {

    fun extractKeys(entity: E): List<Key>

    fun extractNodeInfo(entity: E): List<I>

    fun extractPayload(entity: E): P?

    fun provideRootNodeInfo():I
}
