package tree.api

import java.io.OutputStream

class RenderedNode<R>(val renderedPayload: R,
                      val id: String) {

    var children: List<RenderedNode<R>>? = null

    fun prettyPrint(renderer: (RenderedNode<R>) -> String) {
        prettyPrint(renderer, System.out)
    }

    fun prettyPrint(renderer: (RenderedNode<R>) -> String,
                    outputStream: OutputStream) {

        TreeUtils.prettyPrintRenderedNode(this, renderer, outputStream)
    }
}

