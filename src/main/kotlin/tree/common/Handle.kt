package tree.common

/**
 * This is designed to be immutable.
 */
class Handle private constructor(val reference: String,
                                 val id: String) {

    constructor(reference: String) : this(reference, Hashing.computeHash(reference))
    constructor(references:List<String>):this(references.joinToString("/","/"))

    fun prefaceHandleWith(handle:Handle): Handle {
        return Handle(handle.reference + this.reference)
    }

    companion object{
        val ROOT_HANDLE = Handle("/")
    }
}
