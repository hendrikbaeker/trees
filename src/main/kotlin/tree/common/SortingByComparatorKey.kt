package tree.common

class SortingByComparatorKey<E> (val reference:String, val entitiy: E, val comparator: Comparator<E>) :Key {
    override fun extractReference(): String {
        return reference
    }

    override fun compareTo(other: Key): Int {
        return if(other is SortingByComparatorKey<*>){
            comparator.compare(entitiy, (other as SortingByComparatorKey<E>).entitiy)
        } else{
            -1
        }
    }
}
