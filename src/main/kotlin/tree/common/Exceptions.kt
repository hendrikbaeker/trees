package tree.common

class DuplicateKeyException(override val message: String) : RuntimeException(message)

class MissingParentException(override val message: String) : RuntimeException(message)

class CircularReferenceException(override val message: String) : RuntimeException(message)
