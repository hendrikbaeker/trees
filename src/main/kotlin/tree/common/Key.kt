package tree.common

interface Key : Comparable<Key> {

    fun extractReference(): String
}

