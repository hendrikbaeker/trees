package tree.common

import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.MurmurHash3
import java.nio.ByteBuffer

/**
 * This class provides utilities to compute node references.
 */
object Hashing {
    val LOCAL_ROOT_NODE_REFERENCE = computeLocallyUniqueReference(0, "root")
    val GLOBAL_ROOT_NODE_ID = computeGloballyUniqueReference(listOf("emptyList"))

    /**
     * A node is globally uniquely described by its keyPath, i.e. the keys leading to its contruction. Hence this is suitable information
     * to compute a unique reference.
     */
    fun computeGloballyUniqueReference(keyPath: List<String>): String {

        if(keyPath.isEmpty()){
            return GLOBAL_ROOT_NODE_ID
        }

        val joinedString = java.lang.String.join("/", keyPath)
        return computeHash(joinedString)
    }

    /**
     * A node is locally (i.e. within its level) uniquely described by its key (not necessarily the key path as the first i-1 keys must
     * coincide by construction). This is helpful when putting together node information. This is expected to only depend on the level and
     * the key, not the entire key path.
     */
    fun computeLocallyUniqueReference(keyPath: List<String>): String {
        if(keyPath.isEmpty()){
            return LOCAL_ROOT_NODE_REFERENCE
        }
        return computeHash(keyPath.size.toString() + "/" + keyPath[keyPath.size - 1])
    }

    /**
     * A node is locally (i.e. within its level) uniquely described by its key (not necessarily the key path as the first i-1 keys must
     * coincide by construction). This is helpful when putting together node information. This is expected to only depend on the level and
     * the key, not the entire key path.
     */
    fun computeLocallyUniqueReference(level: Int, key: String): String {
        return computeHash("$level/$key")
    }


    fun computeHash(string: String): String {
        val bytesPerLong = java.lang.Long.SIZE / java.lang.Byte.SIZE
        val hashAsLong = MurmurHash3.hash128(string.toByteArray())
        val first = ByteBuffer.allocate(bytesPerLong).putLong(hashAsLong[0]).array()
        val second = ByteBuffer.allocate(bytesPerLong).putLong(hashAsLong[1]).array()
        val joinedArray = ByteArray(first.size + second.size)
        System.arraycopy(first, 0, joinedArray, 0, first.size)
        System.arraycopy(second, 0, joinedArray, first.size, second.size)
        return Hex.encodeHexString(joinedArray)
    }
}
