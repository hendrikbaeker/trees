package tree.common

import java.io.OutputStream


/**
 * This class represents a node in a tree. (In particular, this class can hold an entire tree.) The most obvious property of such a node is
 * to have children of the same type. This class is meant for internal usage.
 *
 * By "id" we always mean a programmtically generated String which uniquely identifies a node, even in cases trees are attached to
 * each other. An id is deterministically generated and hence stable with respect to idependent generations of the same tree.
 *
 * By "key" we always mean an instance of the class Key, i.e. a comparable object providing a string. Two keys are considered to be equal,
 * if they provide the same string which we will call "reference". In particular the comparability of keys does not affect equality.
 * The reference is always chosen by the user. The uniqueness of the reference depends on the type of tree.
 *
 * - For parent trees
 * a reference is always globally unique, i.e. a tree cannot be generated from a set of entities two of which provide a same reference.
 * Therefore the global reference on the reference of the local key coincide.
 *
 * - For partition trees a reference is only required to be locally unique, i.e.
 *
 *
 */
@Suppress("UNCHECKED_CAST")
class Node<I, P, A>(var handle: Handle,
                    val nodeInfo: I,
                    private val comparable: Key? = null) : Comparable<Node<I, P, A>> {

    var parent: Node<I, P?, A?>? = null

    var children: List<Node<I, P?, A?>> = mutableListOf()

    var aggregationInfo: A? = null

    var payload: P? = null

    fun prefaceHandleWith(prefacingHandle: Handle) {
        val prefacedHandle = handle.prefaceHandleWith(prefacingHandle)
        handle = prefacedHandle
    }

    fun addChild(child: Node<I, P?, A?>) {
        children = children + listOf(child)
        child.parent = this as? Node<I, P?, A?>
    }

    fun copyNodeWithoutChildren(parent: Node<I, P?, A?>?): Node<I, P?, A?> {
        val result = Node<I, P?, A?>(this.handle, this.nodeInfo, this.comparable)
        result.parent = parent
        result.aggregationInfo = this.aggregationInfo
        result.payload = this.payload
        result.children = mutableListOf()
        return result
    }

    fun sortChildren() {
        children = children.sortedBy { it.comparable }
    }

    fun getAllSuccessors(): List<Node<I, P?, A?>> {
        return children + children.flatMap { it.getAllSuccessors() }
    }

    fun isLeaf(): Boolean {
        return children.isEmpty();
    }

    fun prettyPrint(renderer: (Node<I, P?, A?>) -> String) {
        prettyPrint(renderer, System.out)
    }

    fun prettyPrint(renderer: (Node<I, P?, A?>) -> String,
                    outputStream: OutputStream) {

        TreeUtils.prettyPrintNode(this as Node<I, P?, A?>, renderer, outputStream)
    }

    override fun compareTo(other: Node<I, P, A>): Int {
        if (comparable == null || other.comparable == null) {
            return 0
        }
        return comparable.compareTo(other.comparable)
    }

}
