package tree.common

class SortingByIndexKey(val reference: String, val index: Int) : Key {
    override fun extractReference(): String {
        return reference
    }

    override fun compareTo(other: Key): Int {

        return if(other is SortingByIndexKey){
            index.compareTo(other.index)
        }else{
            -1
        }


    }
}
