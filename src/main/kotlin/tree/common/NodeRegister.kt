package tree.common

class NodeRegister< I, P, A>(private val root: Node<I, P?, A?>){

    private val nodes: MutableMap<String, Node<I, P?, A?>> = mutableMapOf()

    init {
        add(root)
    }

    fun add(node: Node<I, P?, A?>) {
        nodes[node.handle.id] = node
    }

    fun getById(id: String): Node<I, P?, A?>? {
        return nodes[id]
    }

    fun getByReference(reference: String): Node<I, P?, A?>? {
        return nodes[Hashing.computeHash(reference)]
    }

    fun getByHandle(handle: Handle): Node<I, P?, A?>? {
        return nodes[handle.id]
    }

    fun remove(node: Node<I, P?, A?>) {
        nodes.remove(node.handle.id)
    }

    fun removeById(id: String) {
        nodes.remove(id)
    }

    fun removeByReference(reference: String) {
        nodes.remove(Hashing.computeHash(reference))
    }

    fun remove(handle: Handle) {
        nodes.remove(handle.id)
    }

}
