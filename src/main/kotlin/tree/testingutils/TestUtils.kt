package tree.testingutils

import tree.common.Node
import tree.api.Tree
import tree.api.RenderedNode
import java.io.ByteArrayOutputStream
import java.util.*


object TestUtils {

    fun <R> areEqual(renderedTree: RenderedNode<R>,
                     assertedTree: List<String>,
                     renderer: (RenderedNode<R>) -> String): Boolean {

        val outputStream = ByteArrayOutputStream()
        renderedTree.prettyPrint(renderer, outputStream)

        return compareNodes(outputStream, assertedTree)
    }

    fun <I, P, A> areEqual(tree: Tree<I, P, A>,
                           assertedTree: List<String>,
                           renderer: (Node<I, P?, A?>) -> String): Boolean {

        val outputStream = ByteArrayOutputStream()
        tree.getRoot().prettyPrint(renderer, outputStream)

        return compareNodes(outputStream, assertedTree)
    }

    fun <I, P, A> areEqual(node: Node<I, P, A>,
                           assertedTree: List<String>,
                           renderer: (Node<I, P?, A?>) -> String): Boolean {

        val outputStream = ByteArrayOutputStream()
        node.prettyPrint(renderer, outputStream)

        return compareNodes(outputStream, assertedTree)
    }

    private fun compareNodes(outputStream: ByteArrayOutputStream,
                             assertedTree: List<String>): Boolean {

        val serializedAssertedTree = assertedTree.joinToString("\n", "", "\n").toByteArray()
        val serializedActualTree = outputStream.toByteArray()

        if (Arrays.equals(serializedAssertedTree, serializedActualTree)) {
            return true
        }

        val logMessage = StringBuilder()
                .append("Test failed. \nAsserted tree was:\n")
                .append(String(serializedAssertedTree))
                .append("\n\nActual tree was: \n")
                .append(String(serializedActualTree))
                .append("\n\nDetails:")
        val asserted = String(serializedAssertedTree).split("\n".toRegex())
        val actual = String(serializedActualTree).split("\n".toRegex())
        if (asserted.size != actual.size) {
            logMessage.append("\nTrees do not have the same size.")
        } else {
            for (index in asserted.indices) {
                if (asserted[index] == actual[index]) {
                    continue
                }
                logMessage.append("\n\nLine ")
                        .append(index + 1)
                        .append(":")
                        .append("\nAsserted: ")
                        .append(asserted[index])
                        .append("\nActual:   ")
                        .append(actual[index])
            }
        }
        println(logMessage.toString())
        return false
    }
}
