package tree.partition

import tree.api.Tree
import tree.common.*
import java.lang.IllegalArgumentException

/**
 * This class contains the actual business logic for building a tree in the partition setup. The class is not meant to be used directly.
 * Instead use the {@link tree.api.TreeFactory} object.
 */
class PartitionTreeFactory<I, P, A>(private val rawNodes: Set<PartitionRawNode<P>>,
                                    private val nodeInformationMap: Map<String, I>,
                                    private val rootNode: Node<I, P?, A?>) {

    private var cappingDepth = Int.MAX_VALUE
    private var nodeRegister = NodeRegister<I, P?, A?>(rootNode)

    fun buildTree(): Tree<I, P?, A?> {

        val uniqueReferenceSequences = rawNodes.map { rawNode -> rawNode.orderedKeys.map { it.extractReference() } }.toSet()
        if (uniqueReferenceSequences.size != rawNodes.size) {
            throw DuplicateKeyException("Raw nodes contain duplicate reference sequences.")
        }

        nodeRegister = NodeRegister(rootNode)
        buildAndAttachChildren(null, rawNodes)
        rawNodes.forEach { it.reset() }

        return Tree(nodeRegister)
    }

    private fun buildAndAttachChildren(parent: Node<I, P?, A?>?, children: Set<PartitionRawNode<P>>) {

        val randomChild = children.random()

        val abstractKeyPath = randomChild.keyPath
        val keyPath = abstractKeyPath.map { it.extractReference() }
        val handle = Handle(keyPath)

        val localId = Hashing.computeLocallyUniqueReference(keyPath)
        val nodeInfo = nodeInformationMap[localId]
                ?: throw IllegalArgumentException("The node information map does not contain info on node " + keyPath.joinToString(","))

        val child = (if (keyPath.isEmpty()) rootNode else Node(handle, nodeInfo, abstractKeyPath.last())) as Node<I, P?, A?>

        nodeRegister.add(child)

        parent?.addChild(child)

        val currentLevel = randomChild.currentLevel

        if (currentLevel >= cappingDepth) {
            child.children = mutableListOf()
        } else if (isLeafNode(children)) {
            child.children = mutableListOf()
            child.payload = children.single().payload

        } else {
            children.groupBy { it.currentReference }
                    .entries
                    .sortedBy { it.key }
                    .map { buildAndAttachChildren(child, it.value.toSet()) }
                    .toSet()
        }
    }

    private fun isLeafNode(unrefinedSuccessors: Set<PartitionRawNode<P>>): Boolean {
        return (unrefinedSuccessors.size <= 1) && (unrefinedSuccessors.single().noReferencesLeft())
    }
}
