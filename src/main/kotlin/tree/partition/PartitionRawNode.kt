package tree.partition

import tree.common.Key


/**
 * This class is meant as a helper class to instantiate a tree. It can be used in the following scenario: Consider a
 * list of entities of some type having an n-part key. If we defined a total ordering on the key components this yields
 * a n-level tree having the entities as leaves and all intermediate nodes are structuring nodes clustering all entities
 * with the same key entry at a given position. The level of the node then corresponds to the position of the key in
 * the total order.
 *
 *
 * The unrefinedLeafNode is simply such a treeable entity. Its references field defines the keys starting from the root
 * node.
 */
class PartitionRawNode<P>(val orderedKeys: List<Key>,
                          val payload: P?) {

    var currentLevel = 0
        private set

    val currentReference: String
        get() {
            currentLevel++
            return orderedKeys[currentLevel - 1].extractReference()
        }

    val keyPath: List<Key>
        get() = orderedKeys.subList(0, currentLevel)

    fun noReferencesLeft(): Boolean {
        return currentLevel == orderedKeys.size
    }

    internal fun reset() {
        currentLevel = 0
    }

}
