package tree.parent

import tree.common.Key

class ParentRawNode<I, P>(val comparableKey: Key,
                          val parentReference: String?,
                          val nodeInfo: I,
                          val payload: P?) {
}
