package tree.parent

import tree.common.Node
import tree.api.Tree
import tree.common.*

class ParentTreeFactory<I, P, A>(
        private val rawNodes: Set<ParentRawNode<I, P?>>,
        private val rootNode: Node<I, P?, A?>) {

    private var nodeRegister = NodeRegister<I, P?, A?>(rootNode)
    private val EMPTY_STRING = ""

    fun buildTree(): Tree<I, P?, A?> {
        buildNodeMappings()
        val tree = Tree<I, P?, A?>(nodeRegister)
        if (rawNodes.size != tree.countChildren()) {
            throw CircularReferenceException("There appears to be a circular exception in the defined entities.")
        }
        tree.sortChildren()
        return tree
    }

    private fun buildNodeMappings() {

        nodeRegister = NodeRegister(rootNode)

        val distinctValues = rawNodes.map { it.comparableKey.extractReference() }.distinct()
        if (distinctValues.size != rawNodes.size) {
            throw DuplicateKeyException("There is at least one duplicate key in the entities set.")
        }

        val workingNodes = rawNodes.map { it.comparableKey.extractReference() to it }.toMap(mutableMapOf())


        while (!workingNodes.isEmpty()) {
            val randomElement = workingNodes.values.random()
            processAndReturnElement(randomElement, workingNodes)
        }
    }

    private fun processAndReturnElement(inputNode: ParentRawNode<I, P?>,
                                        remainingNodes: MutableMap<String, ParentRawNode<I, P?>>): Node<I, P?, A?> {


        val outputNode = buildNodeFromRawAndRegister(inputNode, remainingNodes)

        val parentReference = inputNode.parentReference ?: EMPTY_STRING
        val parentHasStillToBeProcessed = remainingNodes.containsKey(parentReference)

        if (parentHasStillToBeProcessed) {

            val parentNode = remainingNodes[parentReference]!!

            val parent = processAndReturnElement(parentNode, remainingNodes)
            parent.addChild(outputNode)
        } else {
            val parentNode = nodeRegister.getByReference(Handle.ROOT_HANDLE.reference + parentReference)
                    ?: throw MissingParentException("The parent node seems to have been processed, but it is not registered as such. " +
                            "This usually happens, if the specified parent does not exist. " +
                            "nodeReference=${inputNode.comparableKey.extractReference()}, parentReference=$parentReference")

            parentNode.addChild(outputNode)
        }

        return outputNode

    }

    private fun buildNodeFromRawAndRegister(inputNode: ParentRawNode<I, P?>,
                                            remainingNodes: MutableMap<String, ParentRawNode<I, P?>>): Node<I, P?, A?> {

        val inputKey = inputNode.comparableKey
        val nodeHandle = Handle(Handle.ROOT_HANDLE.reference + inputKey.extractReference())
        val outputNode = Node<I, P?, A?>(nodeHandle, inputNode.nodeInfo, inputKey)
        outputNode.payload = inputNode.payload

        remainingNodes.remove(inputKey.extractReference())
        nodeRegister.add(outputNode)

        return outputNode
    }


}


