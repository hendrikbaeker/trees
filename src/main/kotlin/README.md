#Trees

By a tree we mean an acyclic, directed, connected graph. A tree has a unique root element and each element has a possibly empty set of
children. Note that this library is meant to be used for presenting tree structures. Therefore we always tacitly assume that there exists
an order on any set of children and the children are modelled as list.

Any vertex in this graph is called node and any node has four bits of information attached to it:
* a *handle*: The handle can be thought of a node id. Since it must serve different purposes, it is split into
  * a *reference*: This is either directly defined by the user or derived from user defined input
  * an *id*: This is a hash of the reference and meant to be used when doing computations in stages. The user then may be handed a 
  priliminary tree and only later retrieve detailed information on the nodes.
* *node information*: This is a non-optional bit of information the user needs to provide for any node.
* *payload*: This is an optional bit of information. In some cases the user may not be able to provide a payload for every node, this may
remain null then.
* *aggregation info*: see below.

##Tree Types
There are two standard situations in which a tree needs to be constructed.

* ParentTrees
* PartitionTrees

###ParentTrees
On a set one can naturally define a tree structure by endowing each element with an optional parent. This cannot be done at random.
For instance the resulting structure may contain cycles or there may not be a unique root. Still this seems to be a rather canonical
way to define a tree. This library offers the possibility to generate a tree from a set of entities which in some way have a parent.

For this you simply need a list of entities and information on how to extract an id and a parent id. The library will then generate a
tree with an artificial leaf node. I.e. all node without a parent will be treated as children of this artificial node. The library will
detect cycles in the structure and refuse to return a tree.


###PartitionTrees
Consider a collection of entities with no other structure (but the set structure) and assume that each entity is endowed with an n-tupeled
key where n may vary among the entities. On this set one can define a sequence of increasingly fine partitions by considering the first
i keys and collecting all entities for which these first i keys coincide. Moreover, this naturally yields a tree structure where the i-the
 level of nodes corresponds to the i-th partition. Before we continue let's consider an example   

####Example
Consider the example of entities where the keys are accounts and companies:
* A, keys: X/x/0
* B, keys: X/y/0
* C, keys: X/y/1
* D, keys: Y/x
* E, keys: Y/y
* F, keys: Z

The partitions are then given by
```
{{A, B, C}, {D, E}, {F}}
{{A}, {B, C}, {D}, {E}, {F}}
{{A}, {B}, {C}, {D}, {E}, {F}}
```

We expect this to be written as a tree as follows:

```
root
├──X
│  ├──x
│  │  └── 0 : A
│  └──y
│     ├── 0 : B
│     └── 1 : C
├──Y
│  ├──x : D
│  └──y : E
└──Z : F
```

#### Handle
As explained above, the handle of a node contains an id and a reference. In the above example the references of the node would be
```
/
├──/X
│  ├──/X/x
│  │  └── /X/x/0 : A
│  └──/X/y
│     ├── /X/y/0 : B
│     └── /X/y/1 : C
├──/Y
│  ├──/Y/x : D
│  └──/Y/y : E
└──/Z : F
```

The ids are then simply hashes of the references

##Aggregation
Sometimes it is necessary to aggregate information from the leaves of a tree to the root. For instance think of the leaves as some sort
of amounts. These amounts are summed up in multiple steps (corresponding to the levels of the tree) giving a sum on each level. This can be done by adding an
[aggregation config](tree/api/configuration/AggregationConfig.kt) in the builder. The aggregation config essentially contains a method explaining
how to extract aggregation information from the children of a node and aggegating this into this node's aggregation info container.

##Testing and Examples
There exists a small [testing utility class](tree/testingutils/TestUtils.kt) which allows to write human legible test cases. For an example
of how to use this see the [examples](../../test/kotlin/tree). 

###Parent tree
As a quick start consider the following entities

```
val sortedEntitiesWithParents = listOf(
            EntityWithParent("w", "U"),
            EntityWithParent("U", null),
            EntityWithParent("V", null),
            EntityWithParent("x", "U"),
            EntityWithParent("y", "V"),
            EntityWithParent("z", "V"),
            EntityWithParent("0", "w"),
            EntityWithParent("1", "w"),
            EntityWithParent("3", "x"),
            EntityWithParent("2", "x"),
            EntityWithParent("4", "x"))
```
From this we expect the tree
```
/
├──/U
|  ├──/w
|  |  ├──/0
|  |  └──/1
|  └──/x
|     ├──/3
|     ├──/2
|     └──/4
└──/V
   ├──/y
   └──/z
```

which can simply built by
```
val result = TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                entities = sortedEntitiesWithParents,
                extractor = TestBasicParentTreeExtractor())

class TestBasicParentTreeExtractor : BasicParentTreeExtractor<String, Int, EntityWithParent> {

    override fun extractNodeInformation(entity: EntityWithParent): String {
        return entity.reference
    }

    override fun extractPayload(entity: EntityWithParent): Int? {
        return entity.reference.length
    }

    override fun extractParentReference(entity: EntityWithParent): String? {
        return entity.parent
    }

    override fun extractBasicKey(entity: EntityWithParent): String {
        return entity.reference
    }

    override fun provideRootNodeInfo(): String {
        return "rootNode"
    }
}       
```
