package tree

import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import tree.api.TreeFactory
import tree.common.CircularReferenceException
import tree.common.DuplicateKeyException
import tree.common.MissingParentException
import tree.provider.EntityWithParent
import tree.provider.TestBasicParentTreeExtractor
import tree.provider.TestComparableParentTreeExtractor
import tree.provider.TestEntityProvider
import tree.testingutils.TestUtils.areEqual

internal class ParentTests {

    @Test
    internal fun testSortingByList() {

        val result = TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                entities = TestEntityProvider.sortedEntitiesWithParents,
                extractor = TestBasicParentTreeExtractor())

        val assertedTree = listOf(
                "/",
                "├──/U",
                "|  ├──/w",
                "|  |  ├──/0",
                "|  |  └──/1",
                "|  └──/x",
                "|     ├──/3",
                "|     ├──/2",
                "|     └──/4",
                "└──/V",
                "   ├──/y",
                "   └──/z")

        assertTrue(areEqual(result, assertedTree) { node -> node.handle.reference })
    }

    @Test
    internal fun testSortingByComparator() {

        val reverseAlphabetical = Comparator<EntityWithParent> { e1, e2 -> -e1.reference.compareTo(e2.reference) }
        val result = TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                entities = TestEntityProvider.unsortedEntitiesWithParents,
                extractor = TestBasicParentTreeExtractor(),
                comparator = reverseAlphabetical)

        val assertedTree = listOf(
                "/",
                "├──/V",
                "|  ├──/z",
                "|  └──/y",
                "└──/U",
                "   ├──/x",
                "   |  ├──/4",
                "   |  ├──/3",
                "   |  └──/2",
                "   └──/w",
                "      ├──/1",
                "      └──/0"
        )

        assertTrue(areEqual(result, assertedTree) { node -> node.handle.reference })
    }

    @Test
    internal fun testSortingByComparableKey() {

        val result = TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                entities = TestEntityProvider.unsortedEntitiesWithParents,
                extractor = TestComparableParentTreeExtractor())

        val assertedTree = listOf(
                "/",
                "├──/U",
                "|  ├──/w",
                "|  |  ├──/0",
                "|  |  └──/1",
                "|  └──/x",
                "|     ├──/2",
                "|     ├──/3",
                "|     └──/4",
                "└──/V",
                "   ├──/y",
                "   └──/z")

        assertTrue(areEqual(result, assertedTree) { node -> node.handle.reference })
    }

    @Test
    internal fun duplicateKey() {
        val entities = setOf(
                EntityWithParent("x", null),
                EntityWithParent("4", "x"),
                EntityWithParent("4", "x"))

        assertThatThrownBy {
            TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                    entities = entities,
                    extractor = TestBasicParentTreeExtractor(),
                    comparator = Comparator { e1, e2 -> e1.reference.compareTo(e2.reference) }
            )
        }.isInstanceOf(DuplicateKeyException::class.java)
    }

    @Test
    internal fun ambiguousParent() {
        val entities = listOf(
                EntityWithParent("x", null),
                EntityWithParent("4", "x"),
                EntityWithParent("4", "y"))

        assertThatThrownBy {
            TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                    entities = entities,
                    extractor = TestBasicParentTreeExtractor())
        }.isInstanceOf(DuplicateKeyException::class.java)
    }

    @Test
    internal fun parentDoesNotExist() {
        val entities = listOf(
                EntityWithParent("x", null),
                EntityWithParent("4", "y"))

        assertThatThrownBy {
            TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                    entities = entities,
                    extractor = TestBasicParentTreeExtractor())
        }.isInstanceOf(MissingParentException::class.java)
    }

    @Test
    internal fun circularReferences() {
        val entities = listOf(
                EntityWithParent("x", null),
                EntityWithParent("4", "y"),
                EntityWithParent("y", "4"))

        assertThatThrownBy {
            TreeFactory.buildParentTree<String, Int, Int, EntityWithParent>(
                    entities = entities,
                    extractor = TestBasicParentTreeExtractor())
        }.isInstanceOf(CircularReferenceException::class.java)

    }


}
