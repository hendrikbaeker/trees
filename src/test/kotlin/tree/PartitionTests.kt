package tree

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import tree.api.TreeFactory
import tree.common.DuplicateKeyException
import tree.provider.*
import tree.testingutils.TestUtils
import kotlin.test.assertTrue

internal class PartitionTests {

    @Test
    internal fun testHappyCase() {

        val result = TreeFactory.buildPartitionTree<String, Int, Int, Entity>(
                entities = TestEntityProvider.rawNodes,
                extractor = TestKeyExtrationStrategy())

        val assertedNodeInfos = listOf(
                "root",
                "├──A",
                "|  ├──a",
                "|  |  ├──0",
                "|  |  └──1",
                "|  └──b",
                "|     ├──0",
                "|     ├──1",
                "|     └──2",
                "└──B",
                "   ├──a",
                "   └──b"
        )

        assertTrue(TestUtils.areEqual(result, assertedNodeInfos) { node -> node.nodeInfo })

        val assertedReferences = listOf(
                "/",
                "├──/A",
                "|  ├──/A/a",
                "|  |  ├──/A/a/0",
                "|  |  └──/A/a/1",
                "|  └──/A/b",
                "|     ├──/A/b/0",
                "|     ├──/A/b/1",
                "|     └──/A/b/2",
                "└──/B",
                "   ├──/B/a",
                "   └──/B/b"
        )

        assertTrue(TestUtils.areEqual(result, assertedReferences, { node -> node.handle.reference }))
    }

    @Test
    internal fun flawedKeyLengths() {

        Assertions.assertThatThrownBy {
            TreeFactory.buildPartitionTree<String, Int, Int, Entity>(
                    entities = TestEntityProvider.rawNodes,
                    extractor = FlawedExtractionConfig())
        }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageContaining("At least one of the entities provides different numbers of node keys and information")


    }

    @Test
    internal fun duplicateKeys() {
        val rawNodesWithDuplicate = setOf(
                Entity(listOf(StringKey("A"), StringKey("a"), StringKey("0")), 0),
                Entity(listOf(StringKey("A"), StringKey("a"), StringKey("0")), 1))

        Assertions.assertThatThrownBy {
            TreeFactory.buildPartitionTree<String, Int, Int, Entity>(
                    entities = rawNodesWithDuplicate,
                    extractor = TestKeyExtrationStrategy())

        }
                .isInstanceOf(DuplicateKeyException::class.java)
                .hasMessageContaining("Raw nodes contain duplicate reference sequences.")
    }

    @Test
    internal fun testIdsAreReproducible() {

        val firstInstance = TreeFactory.buildPartitionTree<String, Int, Int, Entity>(
                entities = TestEntityProvider.rawNodes,
                extractor = TestKeyExtrationStrategy())

        val secondInstance = TreeFactory.buildPartitionTree<String, Int, Int, Entity>(
                entities = TestEntityProvider.rawNodes,
                extractor = TestKeyExtrationStrategy())

        assertTrue(firstInstance.genericEquals(secondInstance) { node1, node2 -> node1.handle.id == node2.handle.id })

    }
}
