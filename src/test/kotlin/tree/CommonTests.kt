package tree

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import tree.api.RenderedNode
import tree.api.Tree
import tree.api.TreeFactory
import tree.api.configuration.AggregationConfig
import tree.api.configuration.RenderingConfig
import tree.common.Node
import tree.provider.*
import tree.testingutils.TestUtils.areEqual

internal class Tests {

    @Test
    internal fun attachPartitionTreeToParentTree() {

        val parentTree = TestEntityProvider.getDefaultParentTree()
        val partitionTree = TestEntityProvider.getDefaultPartionTree()

        parentTree.attachTreeTo("/4", partitionTree)

        val resultingTree = listOf(
                "/",
                "├──/U",
                "|  ├──/w",
                "|  |  ├──/0",
                "|  |  └──/1",
                "|  └──/x",
                "|     ├──/3",
                "|     ├──/2",
                "|     └──/4",
                "|        ├──/4/A",
                "|        |  ├──/4/A/a",
                "|        |  |  ├──/4/A/a/0",
                "|        |  |  └──/4/A/a/1",
                "|        |  └──/4/A/b",
                "|        |     ├──/4/A/b/0",
                "|        |     ├──/4/A/b/1",
                "|        |     └──/4/A/b/2",
                "|        └──/4/B",
                "|           ├──/4/B/a",
                "|           └──/4/B/b",
                "└──/V",
                "   ├──/y",
                "   └──/z"
        )
        assertTrue(areEqual(parentTree, resultingTree) { node -> node.handle.reference })
    }

    @Test
    internal fun attachParentTreeToPartitionTree() {

        val parentTree = TestEntityProvider.getDefaultParentTree()
        val partitionTree = TestEntityProvider.getDefaultPartionTree()

        partitionTree.attachTreeTo("/A/b/0", parentTree)

        val assertedTree = listOf(
                "/",
                "├──/A",
                "|  ├──/A/a",
                "|  |  ├──/A/a/0",
                "|  |  └──/A/a/1",
                "|  └──/A/b",
                "|     ├──/A/b/0",
                "|     |  ├──/A/b/0/U",
                "|     |  |  ├──/A/b/0/w",
                "|     |  |  |  ├──/A/b/0/0",
                "|     |  |  |  └──/A/b/0/1",
                "|     |  |  └──/A/b/0/x",
                "|     |  |     ├──/A/b/0/3",
                "|     |  |     ├──/A/b/0/2",
                "|     |  |     └──/A/b/0/4",
                "|     |  └──/A/b/0/V",
                "|     |     ├──/A/b/0/y",
                "|     |     └──/A/b/0/z",
                "|     ├──/A/b/1",
                "|     └──/A/b/2",
                "└──/B",
                "   ├──/B/a",
                "   └──/B/b"
        )

        assertTrue(areEqual(partitionTree, assertedTree) { node -> node.handle.reference })

    }

    @Test
    internal fun aggregateInformation() {

        val parentTree = TestEntityProvider.getDefaultParentTree()
        val partitionTree = TestEntityProvider.getDefaultPartionTree()

        parentTree.attachTreeTo("/4", partitionTree)
        parentTree.aggregate(TestAggregationConfig())

        val aggregatedTree = listOf(
                "69",
                "├──67",
                "|  ├──2",
                "|  |  ├──1",
                "|  |  └──1",
                "|  └──65",
                "|     ├──1",
                "|     ├──1",
                "|     └──63",
                "|        ├──15",
                "|        |  ├──1",
                "|        |  |  ├──0",
                "|        |  |  └──1",
                "|        |  └──14",
                "|        |     ├──2",
                "|        |     ├──4",
                "|        |     └──8",
                "|        └──48",
                "|           ├──16",
                "|           └──32",
                "└──2",
                "   ├──1",
                "   └──1"
        )

        assertTrue(areEqual(parentTree, aggregatedTree) { node -> node.aggregationInfo.toString() })

    }

    @Test
    internal fun renderTree() {
        val parentTree = TestEntityProvider.getDefaultParentTree()
        val partitionTree = TestEntityProvider.getDefaultPartionTree()

        parentTree.attachTreeTo("/4", partitionTree)
        parentTree.aggregate(TestAggregationConfig())
        val renderedTree = parentTree.render(TestRenderingConfig())

        val renderedExpectedTree = listOf(
                "/ 69",
                "├──U 67",
                "|  ├──w 2",
                "|  |  ├──0 1",
                "|  |  └──1 1",
                "|  └──x 65",
                "|     ├──3 1",
                "|     ├──2 1",
                "|     └──4 63",
                "|        ├──A 15",
                "|        |  ├──a 1",
                "|        |  |  ├──0 0",
                "|        |  |  └──1 1",
                "|        |  └──b 14",
                "|        |     ├──0 2",
                "|        |     ├──1 4",
                "|        |     └──2 8",
                "|        └──B 48",
                "|           ├──a 16",
                "|           └──b 32",
                "└──V 2",
                "   ├──y 1",
                "   └──z 1"
        )
        assertTrue(areEqual(renderedTree, renderedExpectedTree) { node -> node.renderedPayload })
    }

    @Test
    internal fun stripTree() {
        val partitionTree = TestEntityProvider.getDefaultPartionTree()
        val strippedTree = partitionTree.stripTreeToFirstTwoLevels()

        val assertedCompleteTree = listOf(
                "/",
                "├──/A",
                "|  ├──/A/a",
                "|  |  ├──/A/a/0",
                "|  |  └──/A/a/1",
                "|  └──/A/b",
                "|     ├──/A/b/0",
                "|     ├──/A/b/1",
                "|     └──/A/b/2",
                "└──/B",
                "   ├──/B/a",
                "   └──/B/b"
        )

        val assertedStrippedTree = listOf(
                "/",
                "├──/A",
                "└──/B"
        )

        assertTrue(areEqual(partitionTree, assertedCompleteTree) { node -> node.handle.reference })
        assertTrue(areEqual(strippedTree, assertedStrippedTree) { node -> node.handle.reference })
    }

    @Test
    internal fun attachingToSelf() {
        val parentTree = TestEntityProvider.getDefaultParentTree()

        Assertions.assertThatThrownBy { parentTree.attachTreeTo("/U", parentTree) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageContaining("Tree cannot be attached to itself")
    }

    @Test
    internal fun attachingToNonExistingNode() {
        val firstInstance = TestEntityProvider.getDefaultParentTree()
        val secondInstance = TestEntityProvider.getDefaultParentTree()

        Assertions.assertThatThrownBy { firstInstance.attachTreeTo("/someOtherPlace", secondInstance) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageContaining("Trying to attach a tree to non-existing reference")
    }

    @Test
    internal fun testEquality() {
        val firstTree = TestEntityProvider.getDefaultPartionTree()
        val secondTree: Tree<String, Int?, Int?> = TreeFactory.buildPartitionTree(
                entities = TestEntityProvider.rawNodesWithAlternativePayload,
                extractor = TestKeyExtrationStrategy())

        val strippedTree = firstTree.stripTreeToFirstTwoLevels()

        assertFalse(firstTree.genericEquals(strippedTree) { _, _ -> true })
        assertTrue(firstTree.genericEquals(secondTree) { node1, node2 -> node1.handle.id == node2.handle.id })
        assertFalse(firstTree.genericEquals(secondTree) { node1, node2 -> node1.payload == node2.payload })
    }
}

class TestAggregationConfig : AggregationConfig<String, Int?, Int?> {

    override fun computeNodeAggregationInfo(node: Node<String, Int?, Int?>): Int? {
        if (node.isLeaf()) {
            return node.payload ?: 0
        }

        return node.children
                .map { it.aggregationInfo ?: 0 }
                .sum()
    }
}

class TestRenderingConfig : RenderingConfig<String, Int?, Int?, String> {

    override fun renderNode(node: Node<String, Int?, Int?>): RenderedNode<String> {
        val lastReference = node.handle.reference.split("/").last()
        val formattedReference = if (lastReference.isEmpty()) "/" else lastReference
        val payload = formattedReference + " " + node.aggregationInfo

        return RenderedNode(payload, node.handle.id)
    }

}
