package tree.provider

import tree.api.configuration.PartitionTreeExtractor
import tree.common.Key

class Entity(val keys: List<StringKey>, val payload: Int)

class TestKeyExtrationStrategy : PartitionTreeExtractor< String, Int, Entity>{
    override fun extractKeys(entity: Entity): List<StringKey> {
        return entity.keys
    }

    override fun extractNodeInfo(entity: Entity): List<String> {
        return entity.keys.map { it.reference.split("/").last() }
    }

    override fun extractPayload(entity: Entity): Int? {
        return entity.payload
    }

    override fun provideRootNodeInfo(): String {
        return "root"
    }

}

class FlawedExtractionConfig : PartitionTreeExtractor<String, Int, Entity> {
    override fun extractKeys(entity: Entity): List<StringKey> {
        return entity.keys
    }

    override fun extractNodeInfo(entity: Entity): List<String> {
        val sensibleNodeInfos = entity.keys.map { it.reference.split("/").last() }
        return sensibleNodeInfos + "superflous"
    }

    override fun extractPayload(entity: Entity): Int? {
        return entity.payload
    }

    override fun provideRootNodeInfo(): String {
        return "root"
    }
}
