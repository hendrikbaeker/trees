package tree.provider

import tree.common.Key

class StringKey(var reference:String): Key {

    override fun extractReference(): String {
        return reference
    }

    override fun compareTo(other: Key): Int {
        return reference.compareTo(other.extractReference())
    }

}
