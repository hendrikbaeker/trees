package tree.provider

import tree.api.configuration.ComparableParentTreeExtractor
import tree.api.configuration.BasicParentTreeExtractor
import tree.api.configuration.PartitionTreeExtractor
import tree.common.Key

class EntityWithParent(val reference: String, val parent: String?)

class TestBasicParentTreeExtractor : BasicParentTreeExtractor<String, Int, EntityWithParent> {

    override fun extractNodeInformation(entity: EntityWithParent): String {
        return entity.reference
    }

    override fun extractPayload(entity: EntityWithParent): Int? {
        return entity.reference.length
    }

    override fun extractParentReference(entity: EntityWithParent): String? {
        return entity.parent
    }

    override fun extractBasicKey(entity: EntityWithParent): String {
        return entity.reference
    }

    override fun provideRootNodeInfo(): String {
        return "rootNode"
    }
}

class TestComparableParentTreeExtractor : ComparableParentTreeExtractor<String, Int, EntityWithParent> {

    override fun extractComparableKey(entity: EntityWithParent): StringKey {
        return StringKey(entity.reference)
    }

    override fun extractNodeInformation(entity: EntityWithParent): String {
        return entity.reference
    }

    override fun extractPayload(entity: EntityWithParent): Int? {
        return entity.reference.length
    }

    override fun extractParentReference(entity: EntityWithParent): String? {
        return entity.parent
    }

    override fun provideRootNodeInfo(): String {
        return "rootNode"
    }
}
