package tree.provider

import tree.api.Tree
import tree.api.TreeFactory

object TestEntityProvider {

    val sortedEntitiesWithParents = listOf(
            EntityWithParent("w", "U"),
            EntityWithParent("U", null),
            EntityWithParent("V", null),
            EntityWithParent("x", "U"),
            EntityWithParent("y", "V"),
            EntityWithParent("z", "V"),
            EntityWithParent("0", "w"),
            EntityWithParent("1", "w"),
            EntityWithParent("3", "x"),
            EntityWithParent("2", "x"),
            EntityWithParent("4", "x"))

    val unsortedEntitiesWithParents = setOf(
            EntityWithParent("U", null),
            EntityWithParent("2", "x"),
            EntityWithParent("w", "U"),
            EntityWithParent("y", "V"),
            EntityWithParent("3", "x"),
            EntityWithParent("z", "V"),
            EntityWithParent("V", null),
            EntityWithParent("0", "w"),
            EntityWithParent("1", "w"),
            EntityWithParent("x", "U"),
            EntityWithParent("4", "x"))

    val rawNodes = setOf(
            Entity(listOf(StringKey("A"), StringKey("a"), StringKey("0")), 0),
            Entity(listOf(StringKey("A"), StringKey("a"), StringKey("1")), 1),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("0")), 2),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("1")), 4),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("2")), 8),
            Entity(listOf(StringKey("B"), StringKey("a")), 16),
            Entity(listOf(StringKey("B"), StringKey("b")), 32))

    val rawNodesWithAlternativePayload = setOf(
            Entity(listOf(StringKey("A"), StringKey("a"), StringKey("0")), 1),
            Entity(listOf(StringKey("A"), StringKey("a"), StringKey("1")), 3),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("0")), 9),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("1")), 27),
            Entity(listOf(StringKey("A"), StringKey("b"), StringKey("2")), 81),
            Entity(listOf(StringKey("B"), StringKey("a")), 243),
            Entity(listOf(StringKey("B"), StringKey("b")), 729))


    fun getDefaultPartionTree(): Tree<String, Int?, Int?> {
        return TreeFactory.buildPartitionTree(
                entities = rawNodes,
                extractor = TestKeyExtrationStrategy())
    }

    fun getDefaultParentTree(): Tree<String, Int?, Int?> {
        return TreeFactory.buildParentTree(
                entities = sortedEntitiesWithParents,
                extractor = TestBasicParentTreeExtractor())
    }







}
